package com.example.td0.services;

import com.example.td0.entities.GestionnaireCompte;

import java.util.List;
import java.util.Optional;

public interface GestionnaireCompteService {
    GestionnaireCompte create(GestionnaireCompte gestionnaireCompte);

    Optional<GestionnaireCompte> readOne(Long id);

    List<GestionnaireCompte> readAll();

    void delete(Long id);
}
