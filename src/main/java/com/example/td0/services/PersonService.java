package com.example.td0.services;

import com.example.td0.entities.Person;

import java.util.List;
import java.util.Optional;

public interface PersonService {
    Person create(Person person);

    Optional<Person> readOne(Long id);

    List<Person> readAll();

    void delete(Long id);
}

