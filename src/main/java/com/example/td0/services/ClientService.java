package com.example.td0.services;

import com.example.td0.entities.Client;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    Client create(Client client);

    Optional<Client> readOne(Long id);

    List<Client> readAll();

    void delete(Long id);
}
