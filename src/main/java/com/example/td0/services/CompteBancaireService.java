package com.example.td0.services;

import com.example.td0.entities.CompteBancaire;

import java.util.List;
import java.util.Optional;

public interface CompteBancaireService {
    CompteBancaire create(CompteBancaire compteBancaire);

    Optional<CompteBancaire> readOne(Long id);

    List<CompteBancaire> readAll();

    void delete(Long id);
}
