package com.example.td0.services.impl;

import com.example.td0.entities.Person;
import com.example.td0.repositories.PersonRepository;
import com.example.td0.services.PersonService;

import java.util.List;
import java.util.Optional;

public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person create(Person person) {
        return personRepository.save(person);
    }

    @Override
    public Optional<Person> readOne(Long id) {
        return personRepository.findById(id);
    }

    @Override
    public List<Person> readAll() {
        return personRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        personRepository.deleteById(id);
    }
}
