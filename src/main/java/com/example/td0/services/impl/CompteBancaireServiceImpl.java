package com.example.td0.services.impl;

import com.example.td0.entities.CompteBancaire;
import com.example.td0.repositories.CompteBancaireRepository;
import com.example.td0.services.CompteBancaireService;

import java.util.List;
import java.util.Optional;

public class CompteBancaireServiceImpl implements CompteBancaireService {

    private final CompteBancaireRepository compteBancaireRepository;

    public CompteBancaireServiceImpl(CompteBancaireRepository compteBancaireRepository) {
        this.compteBancaireRepository = compteBancaireRepository;
    }

    @Override
    public CompteBancaire create(CompteBancaire compteBancaire) {
        return compteBancaireRepository.save(compteBancaire);
    }

    @Override
    public Optional<CompteBancaire> readOne(Long id) {
        return compteBancaireRepository.findById(id);
    }

    @Override
    public List<CompteBancaire> readAll() {
        return compteBancaireRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        compteBancaireRepository.deleteById(id);
    }
}
