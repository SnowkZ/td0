package com.example.td0.services.impl;

import com.example.td0.entities.GestionnaireCompte;
import com.example.td0.repositories.GestionnaireCompteRepository;
import com.example.td0.services.GestionnaireCompteService;

import java.util.List;
import java.util.Optional;

public class GestionnaireCompteServiceImpl implements GestionnaireCompteService {

    private final GestionnaireCompteRepository gestionnaireCompteRepository;

    public GestionnaireCompteServiceImpl(GestionnaireCompteRepository gestionnaireCompteRepository) {
        this.gestionnaireCompteRepository = gestionnaireCompteRepository;
    }


    @Override
    public GestionnaireCompte create(GestionnaireCompte gestionnaireCompte) {
        return gestionnaireCompteRepository.save(gestionnaireCompte);
    }

    @Override
    public Optional<GestionnaireCompte> readOne(Long id) {
        return gestionnaireCompteRepository.findById(id);
    }

    @Override
    public List<GestionnaireCompte> readAll() {
        return gestionnaireCompteRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        gestionnaireCompteRepository.deleteById(id);
    }
}
