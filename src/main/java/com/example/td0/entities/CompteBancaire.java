package com.example.td0.entities;

import com.example.td0.entities.enumeration.TypeCompte;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "compte_bancaire")
public class CompteBancaire implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "compte")
    private TypeCompte compte;

    @Column(name = "solde")
    private double solde;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Client idClient;

    public CompteBancaire() {
    }

    public CompteBancaire(Long id, TypeCompte compte, double solde, Client client) {
        this.id = id;
        this.compte = compte;
        this.solde = solde;
        this.idClient = client;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeCompte getCompte() {
        return compte;
    }

    public void setCompte(TypeCompte compte) {
        this.compte = compte;
    }

    public double getSolde() {
        return solde;
    }

    public void setSoldeInitial(double solde) {
        this.solde = solde;
    }

    public Client getIdClient() {
        return idClient;
    }

    public void setIdClient(Client idClient) {
        this.idClient = idClient;
    }
}
