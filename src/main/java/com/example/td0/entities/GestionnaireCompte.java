package com.example.td0.entities;

import com.example.td0.entities.enumeration.Sexe;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "gestionnaire_compte")
public class GestionnaireCompte extends Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    @NonNull
    private Sexe sexe;

    @Column(name = "bureau")
    private int bureau;

    @Column(name = "identifiant")
    private String identifiant;

    @Transient
    private String password;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Client> clients = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_PK", nullable = false)
    private Person person;

    public GestionnaireCompte() {
    }

    public GestionnaireCompte(Long id, String nom, String prenom, String address, String phone, String email, Sexe sexe, int bureau, String identifiant, String password, List<Client> clients) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.sexe = sexe;
        this.bureau = bureau;
        this.identifiant = identifiant;
        this.password = password;
        this.clients = clients;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String getPrenom() {
        return prenom;
    }

    @Override
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    @NonNull
    public Sexe getSexe() {
        return sexe;
    }

    @Override
    public void setSexe(@NonNull Sexe sexe) {
        this.sexe = sexe;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public int getBureau() {
        return bureau;
    }

    public void setBureau(int bureau) {
        this.bureau = bureau;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Client> getClients() {
        return clients;
    }

    public void setClients(List<Client> clients) {
        this.clients = clients;
    }
}
