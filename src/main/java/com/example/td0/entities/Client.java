package com.example.td0.entities;

import com.example.td0.entities.enumeration.Matrimonial;
import com.example.td0.entities.enumeration.Sexe;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "client")
public class Client extends Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "address")
    private String address;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    @NonNull
    private Sexe sexe;

    @Column(name = "salary")
    private double salary;

    @Enumerated(EnumType.STRING)
    @Column(name = "matrimonial")
    @NonNull
    private Matrimonial status;

    @Column(name = "year_of_birth")
    private int yearOfBirth;

    @Column(name = "code")
    private int code;

    @OneToMany(cascade = CascadeType.ALL)
    private List<CompteBancaire> comptes = new ArrayList<>();



    public Client() {

    }

    public Client(long id, String nom, String prenom, String address, String phone, String email, Sexe sexe, double salary, Matrimonial status, int yearOfBirth, int code ) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.sexe = sexe;
        this.salary = salary;
        this.status = status;
        this.yearOfBirth = yearOfBirth;
        this.code = code;

    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNom() {
        return nom;
    }

    @Override
    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String getPrenom() {
        return prenom;
    }

    @Override
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    @NonNull
    public Sexe getSexe() {
        return sexe;
    }

    @Override
    public void setSexe(@NonNull Sexe sexe) {
        this.sexe = sexe;
    }

    public List<CompteBancaire> getComptes() {
        return comptes;
    }

    public void setComptes(List<CompteBancaire> comptes) {
        this.comptes = comptes;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @NonNull
    public Matrimonial getStatus() {
        return status;
    }

    public void setStatus(@NonNull Matrimonial status) {
        this.status = status;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
