package com.example.td0.repositories;

import com.example.td0.entities.Client;
import com.example.td0.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {

}
