package com.example.td0.repositories;

import com.example.td0.entities.GestionnaireCompte;
import com.example.td0.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GestionnaireCompteRepository extends JpaRepository<GestionnaireCompte, Long> {
}
