package com.example.td0.repositories;

import com.example.td0.entities.CompteBancaire;
import com.example.td0.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteBancaireRepository extends JpaRepository<CompteBancaire, Long> {
}
