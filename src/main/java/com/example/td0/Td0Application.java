package com.example.td0;

import com.example.td0.entities.Client;
import com.example.td0.entities.CompteBancaire;
import com.example.td0.entities.Person;
import com.example.td0.entities.enumeration.Sexe;
import com.example.td0.entities.enumeration.TypeCompte;
import com.example.td0.services.impl.ClientServiceImpl;
import com.example.td0.services.impl.PersonServiceImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class Td0Application {
    private final Logger log = LoggerFactory.getLogger(Td0Application.class);

    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private ClientServiceImpl clientService;

    public static void main(String[] args) {
        SpringApplication.run(Td0Application.class, args);
    }

    @Bean
    void crudClient() {
        CompteBancaire cp = new CompteBancaire();
        cp.setCompte(TypeCompte.CHEQUE);
        cp.setSoldeInitial(2000.00);
        List<CompteBancaire> comptes = new ArrayList<>();
        comptes.add(cp);



        Client client = new Client();
        client.setNom("Marc-Olivier");
        client.setPrenom("Aubé");
        client.setAddress("Random addresse 123, soleil");
        client.setEmail("random@email.com");
        client.setPhone("123-4567-8909");
        client.setSexe(Sexe.Homme);
        client = clientService.create(client);
        log.info("****************** save person : {}", client);

        Optional<Client> userRecover = clientService.readOne(client.getId());
        if (userRecover.isPresent()) {
            log.info("****************** Read one user : {}", client);
        }
    }
}
